# TFA Headless
Provides headless TFA support for Google Authenticator, using the [TFA](https://www.drupal.org/project/tfa) Module.

## Features
The module exists of 4 endpoints:

### /api/totp/generate
Generates an uri for a QR code and a seed.

### /api/totp/status
Checks the status of the user, if TFA is enabled or not.

### /api/totp/register
Registers the user so it can use TFA

### /api/totp/login
Checks if the user is authorized to login with the given code.

## Setup
 * `composer require drupal/tfa_headless`
 * `drush en tfa_headless`
The endpoints can be enabled under `Settings > Webservices > REST`.
Don't forget to set the necessary permissions per endpoint to the desired roles.

## Additional Requirements
Depends on the module [TFA](https://www.drupal.org/project/tfa).
Uses REST to provide the endpoints.
[RESTUI](https://www.drupal.org/project/restui) is a nice to have. This projects enables it by default.
