<?php

namespace Drupal\tfa_headless\Service;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Component\Utility\Crypt;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\encrypt\EncryptionProfileManagerInterface;
use Drupal\encrypt\EncryptServiceInterface;
use Drupal\tfa\TfaUserDataTrait;
use Drupal\user\UserDataInterface;
use Otp\Otp;
use ParagonIE\ConstantTime\Encoding;

/**
 * The service for Headless TFA support.
 */
class TfaHeadlessService {
  use TfaUserDataTrait;

  /**
   * Un-encrypted seed.
   *
   * @var string
   */
  protected $seed;
  /**
   * The tfa config.
   *
   * @var mixed
   */
  protected $tfaConfig;
  /**
   * Encryption profile.
   *
   * @var \Drupal\encrypt\EncryptionProfileInterface
   */
  protected $encryptionProfile;
  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;
  /**
   * The encrypt service.
   *
   * @var \Drupal\encrypt\EncryptServiceInterface
   */
  protected $encryptService;
  /**
   * The encryption profile manager.
   *
   * @var \Drupal\encrypt\EncryptionProfileManagerInterface
   */
  protected $encryptionProfileManager;
  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;
  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * Constructs a new TfaHeadlessService object.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    AccountInterface $current_user,
    UserDataInterface $user_data,
    EncryptionProfileManagerInterface $encryption_profile_manager,
    EncryptServiceInterface $encrypt_service,
    TimeInterface $time,
  ) {
    $this->configFactory = $config_factory;
    $this->tfaConfig = $config_factory->get('tfa.settings')->get('validation_plugin_settings')['tfa_totp'];
    $this->currentUser = $current_user;
    $this->userData = $user_data;
    $this->encryptionProfileManager = $encryption_profile_manager;
    $this->encryptService = $encrypt_service;
    $this->time = $time;
    $this->seed = $this->getSeed();
  }

  /**
   * Validate the TFA code.
   *
   * @param string $code
   *   The TFA code.
   * @param string|null $seed
   *   The TFA seed.
   *
   * @return bool
   *   Is code valid.
   */
  public function validate(string $code, string $seed = NULL): bool {
    $code = preg_replace('/\s+/', '', $code);
    $current_window_base = floor((time() / 30)) - $this->tfaConfig['time_skew'];

    $otp = new Otp();
    $token_valid = (($seed ? $seed : $this->seed) && ($validated_window = $otp->checkHotpResync(
      Encoding::base32DecodeUpper($seed ? $seed : $this->seed),
      $current_window_base,
      $code,
      $this->tfaConfig['time_skew'] * 2
    )));
    if ($token_valid) {
      $this->setUserData(
        'tfa',
        ['tfa_totp_time_window' => $validated_window],
        $this->currentUser->id(), $this->userData
      );
    }
    return $token_valid;
  }

  /**
   * Get seed for this account.
   *
   * @return string
   *   Decrypted account OTP seed or FALSE if none exists.
   */
  public function getSeed(): string {
    // Lookup seed for account and decrypt.
    $result = $this->getUserData('tfa', 'tfa_totp_seed', $this->currentUser->id(), $this->userData);

    if (!empty($result)) {
      $encrypted = base64_decode($result['seed']);
      $seed = $this->decrypt($encrypted);
      if (!empty($seed)) {
        return $seed;
      }
    }
    return FALSE;
  }

  /**
   * Store validated code to prevent replay attack.
   *
   * @param string $code
   *   The validated code.
   */
  public function storeAcceptedCode(string $code): void {
    $code = preg_replace('/\s+/', '', $code);
    $hash = Crypt::hashBase64($code);

    // Store the hash made using the code in users_data.
    $store_data = ['tfa_accepted_code_' . $hash => $this->time->getRequestTime()];
    $this->setUserData('tfa', $store_data, $this->currentUser->id(), $this->userData);
  }

  /**
   * Save seed for account.
   *
   * @param string $seed
   *   Un-encrypted seed.
   */
  public function storeSeed(string $seed): void {
    // Encrypt seed for storage.
    $encrypted = $this->encrypt($seed);

    $record = [
      'tfa_totp_seed' => [
        'seed' => base64_encode($encrypted),
        'created' => $this->time->getRequestTime(),
      ],
    ];

    $this->setUserData('tfa', $record, $this->currentUser->id(), $this->userData);
  }

  /**
   * Encrypt a plaintext string.
   *
   * Should be used when writing codes to storage.
   *
   * @param string $data
   *   The string to be encrypted.
   *
   * @return string
   *   The encrypted string.
   *
   * @throws \Drupal\encrypt\Exception\EncryptException
   */
  public function encrypt(string $data): string {
    $encryptionProfileId = $this->configFactory->get('tfa.settings')->get('encryption');

    return $this->encryptService->encrypt(
      $data,
      $this->encryptionProfileManager->getEncryptionProfile($encryptionProfileId)
    );
  }

  /**
   * Decrypt a encrypted string.
   *
   * Should be used when reading codes from storage.
   *
   * @param string $data
   *   The string to be decrypted.
   *
   * @return string
   *   The decrypted string.
   *
   * @throws \Drupal\encrypt\Exception\EncryptionMethodCanNotDecryptException
   * @throws \Drupal\encrypt\Exception\EncryptException
   */
  public function decrypt(string $data): string {
    $encryptionProfileId = $this->configFactory->get('tfa.settings')->get('encryption');

    return $this->encryptService->decrypt(
      $data,
      $this->encryptionProfileManager->getEncryptionProfile($encryptionProfileId)
    );
  }

  /**
   * Getter for user data.
   *
   * @return \Drupal\user\UserData
   *   The user data.
   */
  public function getGlobalUserData() {
    return $this->userData;
  }

}
