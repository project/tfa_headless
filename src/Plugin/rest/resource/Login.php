<?php

namespace Drupal\tfa_headless\Plugin\rest\resource;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Session\AccountInterface;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Drupal\tfa\TfaUserDataTrait;
use Drupal\tfa_headless\Service\TfaHeadlessService;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Login the user with TFA.
 *
 * @RestResource(
 *   id = "tfa_headless_login",
 *   label = @Translation("TFA Headless Login"),
 *   uri_paths = {
 *     "create" = "/api/totp/login"
 *   }
 * )
 */
class Login extends ResourceBase {
  use TfaUserDataTrait;

  /**
   * The tfa config.
   *
   * @var mixed
   */
  protected $tfaConfig;
  /**
   * Encryption profile.
   *
   * @var \Drupal\encrypt\EncryptionProfileInterface
   */
  protected $encryptionProfile;
  /**
   * The Headless TFA service.
   *
   * @var \Drupal\tfa_headless\Service\TfaHeadlessService
   */
  protected $tfaService;
  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * Constructs a new Login object.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    array $plugin_definition,
    array $serializer_formats,
    LoggerInterface $logger,
    TfaHeadlessService $tfa_service,
    AccountInterface $current_user,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);

    $this->currentUser = $current_user;
    $this->tfaService = $tfa_service;
    $this->userData = $this->tfaService->getGlobalUserData();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('rest'),
      $container->get('tfa_headless.service'),
      $container->get('current_user')
    );
  }

  /**
   * Responds to POST requests.
   *
   * @return \Drupal\rest\ResourceResponse
   *   The HTTP response object.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   *   Throws exception expected.
   */
  public function post(Request $request) {
    $response = NULL;
    $content = Json::decode($request->getContent());
    $code = $content['code'];

    $tfaData = $this->tfaGetTfaData($this->currentUser->id(), $this->userData);
    $enabled = isset($tfaData['status']) && $tfaData['status'] && !empty($tfaData['data']['plugins']);

    if (!$enabled) {
      $response = new ResourceResponse($this->t('TFA is not enabled for this account.'), 401);
    }
    if (!$this->tfaService->validate($code)) {
      $response = new ResourceResponse($this->t('Invalid application code. Please try again.'), 401);
    }

    if (!$response) {
      $response = new ResourceResponse($this->t('OK'), 200);
      $this->tfaService->storeAcceptedCode($code);
    }

    // Disable cache.
    $cache = new CacheableMetadata();
    $cache->setCacheMaxAge(0);
    $response->addCacheableDependency($cache);

    return $response;
  }

}
