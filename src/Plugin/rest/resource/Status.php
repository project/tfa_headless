<?php

namespace Drupal\tfa_headless\Plugin\rest\resource;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Session\AccountInterface;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Drupal\tfa\TfaLoginContextTrait;
use Drupal\user\UserDataInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Get the status of the current user's 2fa state.
 *
 * @RestResource(
 *   id = "tfa_headless_status",
 *   label = @Translation("TFA Headless Status"),
 *   uri_paths = {
 *     "canonical" = "/api/totp/status"
 *   }
 * )
 */
class Status extends ResourceBase {
  use TfaLoginContextTrait;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * Constructs a new Status object.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    array $plugin_definition,
    array $serializer_formats,
    LoggerInterface $logger,
    AccountInterface $current_user,
    UserDataInterface $user_data,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);

    $this->currentUser = $current_user;
    $this->userData = $user_data;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('rest'),
      $container->get('current_user'),
      $container->get('user.data')
    );
  }

  /**
   * Responds to GET requests.
   *
   * @return \Drupal\rest\ResourceResponse
   *   The HTTP response object.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   *   Throws exception expected.
   */
  public function get() {
    $tfaData = $this->tfaGetTfaData($this->currentUser->id(), $this->userData);
    $enabled = isset($tfaData['status']) && $tfaData['status'];

    $response = new ResourceResponse(['status' => $enabled && !empty($tfaData['data']['plugins'])], 200);

    // Disable cache.
    $cache = new CacheableMetadata();
    $cache->setCacheMaxAge(0);
    $response->addCacheableDependency($cache);

    return $response;
  }

}
