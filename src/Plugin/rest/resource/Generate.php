<?php

namespace Drupal\tfa_headless\Plugin\rest\resource;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Otp\GoogleAuthenticator;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Generate a QR code for the frontend.
 *
 * @RestResource(
 *   id = "tfa_headless_generate",
 *   label = "TFA Headless Generate",
 *   uri_paths = {
 *     "canonical" = "/api/totp/generate"
 *   }
 * )
 */
class Generate extends ResourceBase {

  /**
   * The seed.
   *
   * @var string
   */
  protected $seed;
  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;
  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * Constructs a new Generate object.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    array $plugin_definition,
    array $serializer_formats,
    LoggerInterface $logger,
    ConfigFactoryInterface $config_factory,
    AccountInterface $current_user,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);
    $this->currentUser = $current_user;
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('rest'),
      $container->get('config.factory'),
      $container->get('current_user')
    );
  }

  /**
   * Responds to GET requests.
   *
   * @return \Drupal\rest\ResourceResponse
   *   The HTTP response object.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   *   Throws exception expected.
   */
  public function get() {
    $response = new ResourceResponse([
      'uri' => $this->getQrCodeUri(),
      'seed' => $this->seed,
    ], 200);
    // Disable cache.
    $cache = new CacheableMetadata();
    $cache->setCacheMaxAge(0);
    $response->addCacheableDependency($cache);

    return $response;
  }

  /**
   * Generate a seed for OTP secret key.
   */
  private function generateSeed() {
    $ga = new GoogleAuthenticator();
    $this->seed = $ga->generateRandom();
  }

  /**
   * Get a qrcode uri of seed.
   *
   * @return string
   *   QR-code uri.
   */
  private function getQrCodeUri() {
    $tfa_config = $this->configFactory->get('tfa.settings')->get('validation_plugin_settings')['tfa_totp'];
    $this->generateSeed();

    return 'otpauth://totp/' . $this->accountName($tfa_config) . '?secret=' . $this->seed . '&issuer=' . $tfa_config['issuer'];
  }

  /**
   * Get account name for QR image.
   *
   * @param mixed $tfa_config
   *   The tfa settings.
   *
   * @return string
   *   URL encoded string.
   */
  private function accountName($tfa_config) {
    $site_name = (string) $this->configFactory->get('system.site')->get('name');
    $prefix = $tfa_config['site_name_prefix']
      ? preg_replace('@[^a-z0-9-]+@', '-', strtolower($site_name))
      : $tfa_config['name_prefix'];

    return urlencode((!empty($prefix) && $prefix . '-') . $this->currentUser->getAccountName());
  }

}
